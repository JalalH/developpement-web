


fonctionIf()
fonctionFor()


//// Exercice 1
// Chaine
var text = "String"
var array = ['Apple', 'Strawberry', 'Orange', 'Banana']
afficherTableau(text, array)
function afficherTableau(text, array) {
    document.write(text)

    document.write('<table width="80%" cellpadding="10" border="1">')
    document.write('<tr>')
    document.write('<td>' + array[0] + '</td>')
    document.write('<td>' + array[1] + '</td>')
    document.write('<td>' + array[2] + '</td>')
    document.write('<td>' + array[3] + '</td>')
    document.write('</tr>')
    document.write('</table>')
}

/// Exercice 3
// myFunction()
function myFunction(text) {
    alert(text);
  }

//// Exercice 5
function fonctionIf() {
    var variable = 11
    if (variable < 10) {
        alert("La valeur de la fonctionIf est " + variable + ": elle est inferieure a 10")
    } else {
        alert("La valeur de la fonctionIf est " + variable + ": elle est superieure a 10")
    }
}
function fonctionFor() {
    document.write('<table width="80%" cellpadding="10" border="1">')
    for (let step = 0; step <= 10; step++) {
        document.write('<tr>')
        for (let step2 = 0; step2 <= 10; step2++) {
            document.write('<td>' + step + "," + step2 + '</td>')
        }
        document.write('</tr>')
    }
    document.write('</table>')
}

