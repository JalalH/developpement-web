import React, { Children, Component } from 'react';
import logo from './logo.svg';
import './App.css'; 

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedRegion: '', selectedDep: '', items: []};
  }

  myChangeHandlerRegion = (event) => {
    this.setState({ selectedRegion: event.target.value.split(" ")[0] });
    
    var textt =  "https://geo.api.gouv.fr/regions/" + event.target.value.split(" ")[0] + "/departements"
    fetch(textt)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
          this.injectDep(result);
        },
        // Remarque : il est important de traiter les erreurs ici
        // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
        // des exceptions provenant de réels bugs du composant.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
      .catch(console.log)
  }

  injectDep = (listt) => {
    var texxxx = "<option value=\"0\"></option>";
    for (var i = 0; i < listt.length; i++) {
  
      texxxx = texxxx + "<option value=" + listt[i].code + ">" + listt[i].nom + "</option>";
    }
    document.getElementById('test2').innerHTML = texxxx;
  }

  myChangeHandlerDep = (event) => {
    this.setState({ selectedDep: event.target.value });
  }

  myChangeHandlerEtab = (event) => {
    this.setState({ selectedEtab: event.target.value });
  }

  researchAdmin = async() => {
    var textt = "https://etablissements-publics.api.gouv.fr/v3/departements/" + this.state.selectedDep + "/" + this.state.selectedEtab
    var tex = ""
    // fetch(textt)
    //   .then(res => res.json())
    //   .then(
    //     (result) => {
    //       this.setState({
    //         isLoaded: true,
    //         rss_items: result
    //       });
    //       console.log("result: " + result.properties[0].nom);
    //       for (var i = 0; i < result.length; i++) {
          
    //         console.log(i);
    //       }
    //       document.getElementById('RSS').innerHTML = tex;
    //     }, 
    //     // Remarque : il est important de traiter les erreurs ici
    //     // au lieu d'utiliser un bloc catch(), pour ne pas passer à la trappe
    //     // des exceptions provenant de réels bugs du composant.
    //     (error) => {
    //       this.setState({
    //         isLoaded: true,
    //         error
    //       });
    //     }
    //   )
    //   .catch(console.log)

    try {
      let response = await fetch(textt);

      let data = await response.json();

      this.setState({

        data: data.features,

        error: ''

      });

      console.log("result: " + data.features[0].props);

    } catch(e) {

      this.setState({ error: "Erreur lors de la recherche" });

    }
  }

  render() {
    return (
     
      <div className="App">
        <h1>Mon Projet React</h1>
        <br></br> <br></br> <br></br>
        <p>Choisissez votre Region</p>
        <select id="test" onChange={this.myChangeHandlerRegion}>
        <option></option><option>01 - Guadeloupe</option><option>02 - Martinique</option><option>03 - Guyane</option><option>04 - La Réunion</option><option>06 - Mayotte</option><option>11 - Île-de-France</option><option>24 - Centre-Val de Loire</option><option>27 - Bourgogne-Franche-Comté</option><option>28 - Normandie</option><option>32 - Hauts-de-France</option><option>44 - Grand Est</option><option>52 - Pays de la Loire</option><option>53 - Bretagne</option><option>75 - Nouvelle-Aquitaine</option><option>76 - Occitanie</option><option>84 - Auvergne-Rhône-Alpes</option><option>93 - Provence-Alpes-Côte d'Azur</option><option>94 - Corse</option>
        </select>
     
        <div className="Departement">
          <p>Choisissez votre Departement</p>
          <select id="test2" onChange={this.myChangeHandlerDep}>
          </select>
        </div>

        <div className="Etablissement">
          <p>Choisissez l'etablissement</p>
          <select id="test3" onChange={this.myChangeHandlerEtab}>
            <option value="0"></option><option value="accompagnement_personnes_agees">Plateforme d'accompagnement et de répit pour les aidants de personnes âgées</option><option value="adil">Agence départementale pour l’information sur le logement</option><option value="afpa">Association nationale pour la formation professionnelle des adultes</option><option value="anah">Agence nationale de l’habitat</option><option value="apec">Association pour l’emploi des cadres</option><option value="apecita">Association pour l'emploi des cadres, ingénieurs et techniciens de l'agriculture et de l'agroalimentaire</option><option value="ars">Agence régionale de santé</option><option value="ars_antenne">Délégation territoriale de l'Agence régionale de santé</option><option value="banque_de_france">Banque de France, succursale</option><option value="bav">Bureau d'aide aux victimes</option><option value="bsn">Bureau ou centre du service national</option><option value="caa">Cour administrative d’appel</option><option value="caf">Caisse d’allocations familiales</option><option value="carsat">Caisse d'assurance retraite et de la santé au travail</option><option value="ccas">Centre communal d'action sociale</option><option value="cci">Chambre de commerce et d’industrie</option><option value="cdas">Centre départemental d'action sociale</option><option value="cddp">Centre départemental de documentation pédagogique</option><option value="cdg">Centre de gestion de la fonction publique territoriale</option><option value="centre_detention">Centre de détention</option><option value="centre_impots_fonciers">Centre des impôts foncier et cadastre</option><option value="centre_penitentiaire">Centre pénitentiaire</option><option value="centre_social">Centre social</option><option value="cesr">Conseil économique, social et environnemental régional</option><option value="cg">Conseil départemental</option><option value="chambre_agriculture">Chambre d’agriculture</option><option value="chambre_metier">Chambre de métiers et de l’artisanat</option><option value="cicas">Centre d’information de conseil et d'accueil des salariés</option><option value="cidf">Centre d’information sur les droits des femmes et des familles</option><option value="cij">Information jeunesse</option><option value="cio">Centre d’information et d’orientation</option><option value="civi">Commission d'indemnisation des victimes d'infraction</option><option value="clic">Point d'information local dédié aux personnes âgées</option><option value="cnfpt">Centre national de la fonction publique territoriale</option><option value="cnra">Centre en route de la navigation aérienne</option><option value="commissariat_police">Commissariat de police</option><option value="commission_conciliation">Commission départementale de conciliation</option><option value="conciliateur_fiscal">Conciliateur fiscal</option><option value="conseil_culture">Conseil de la culture, de l’éducation et de l’environnement</option><option value="cour_appel">Cour d'appel</option><option value="cpam">Caisse primaire d’assurance maladie</option><option value="cr">Conseil régional</option><option value="crc">Chambre régionale ou territoriale des comptes</option><option value="crdp">Centre régional de documentation pédagogique</option><option value="creps">Centre régional d’éducation populaire et de sports</option><option value="crfpn">Centre ou délégation régionale de recrutement et de formation de la police nationale</option><option value="crib">Centre de ressources et d'information des bénévoles</option><option value="crous">Crous</option><option value="csl">Centre de semi-liberté</option><option value="dac">Direction de l’aviation civile</option><option value="dd_femmes">Droit des femmes et égalité</option><option value="dd_fip">Direction départementale des finances publiques</option><option value="ddcs">Direction départementale de la cohésion sociale</option><option value="ddcspp">Direction départementale de la cohésion sociale et de la protection des populations</option><option value="ddpjj">Direction territoriale de la protection judiciaire de la jeunesse</option><option value="ddpp">Protection des populations</option><option value="ddsp">Direction départementale ou service de la sécurité publique</option><option value="ddt">Direction départementale des territoires et de la mer</option><option value="defenseur_droits">Défenseur des droits</option><option value="did_routes">Direction interdépartementale des routes</option><option value="dir_mer">Direction interrégionale de la mer</option><option value="dir_meteo">Météo France</option><option value="dir_pj">Direction interrégionale de la police judiciaire</option><option value="direccte">Direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi</option><option value="direccte_ut">Unité territoriale - Direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi</option><option value="dml">Délégation à la mer et au littoral</option><option value="dr_femmes">Délégation régionale aux droits des femmes et à l’égalité</option><option value="dr_fip">Direction régionale des finances publiques</option><option value="dr_insee">Délégation régionale de l’INSEE</option><option value="drac">Direction régionale des affaires culturelles</option><option value="draf">Direction régionale de l'alimentation, de l’agriculture et de la forêt</option><option value="drddi">Direction interrégionale et régionale des douanes</option><option value="dreal">Direction régionale de l’environnement, de l’aménagement et du logement</option><option value="dreal_ut">Unité territoriale - Direction régionale de l'environnement, de l'aménagement et du logement</option><option value="driea">Direction régionale et interdépartementale de l'équipement et de l'aménagement</option><option value="driea_ut">Unité territoriale - Direction régionale et interdépartementale de l'équipement et de l'aménagement</option><option value="driee">Direction régionale et interdépartementale de l'environnement et de l'énergie</option><option value="driee_ut">Unité territoriale - Direction régionale et interdépartementale de l'environnement et de l'énergie</option><option value="drihl">Direction régionale et interdépartementale de l'hébergement et du Hébergement et logement</option><option value="drihl_ut">Unité territoriale - Direction régionale et interdépartementale de l'hébergement et du logement</option><option value="drjscs">Direction régionale de la jeunesse, des sports et de la cohésion sociale</option><option value="dronisep">Délégation régionale de l’ONISEP</option><option value="drpjj">Direction interdépartementale ou régionale de la protection judiciaire de la jeunesse</option><option value="drrt">Délégation régionale à la recherche et à la technologie</option><option value="drsp">Direction interrégionale des services pénitentiaires</option><option value="dz_paf">Direction zonale de la police aux frontières</option><option value="edas">Établissement départemental d'actions sociales</option><option value="epci">Intercommunalité</option><option value="esm">Etablissement spécialisé pour mineurs</option><option value="fdapp">Fédération départementale pour la pêche et la protection du milieu aquatique</option><option value="fdc">Fédération départementale des chasseurs</option><option value="fongecif">Fongecif</option><option value="gendarmerie">Brigade de gendarmerie</option><option value="greta">Greta</option><option value="hypotheque">Service de publicité foncière ex-conservation des hypothèques</option><option value="inspection_academique">Direction des services départementaux de l'Éducation nationale</option><option value="maia">Mission d’accueil et d’information des associations</option><option value="mairie">Mairie</option><option value="mairie_com">Mairie des collectivités d'outre-mer</option><option value="mairie_mobile">Mairie mobile de la ville de Paris</option><option value="maison_arret">Maison d'arrêt</option><option value="maison_centrale">Maison centrale</option><option value="maison_handicapees">Maison départementale des personnes handicapées</option><option value="mds">Maison départementale des solidarités</option><option value="mission_locale">Mission locale et Permanence d’accueil, d’information et d’orientation</option><option value="mjd">Maison de justice et du droit</option><option value="msa">Mutualité sociale agricole</option><option value="ofii">Office français de l'immigration et de l'intégration</option><option value="onac">Office national des anciens combattants</option><option value="onf">Direction régionale de l'Office national des forêts</option><option value="paris_mairie">Mairie de Paris, Hôtel de Ville</option><option value="paris_mairie_arrondissement">Mairie de Paris, mairie d'arrondissement</option><option value="paris_ppp">Préfecture de police de Paris</option><option value="paris_ppp_antenne">Préfecture de police de Paris, antenne d’arrondissement</option><option value="paris_ppp_certificat_immatriculation">Préfecture de police de Paris, certificat d'immatriculation</option><option value="paris_ppp_gesvres">Préfecture de police de Paris - Site central de Gesvres</option><option value="paris_ppp_permis_conduire">Préfecture de police de Paris, permis de conduire</option><option value="permanence_juridique">Permanence juridique</option><option value="pif">Point info famille</option><option value="pmi">Centre de protection maternelle et infantile</option><option value="pole_emploi">Pôle emploi</option><option value="pp_marseille">Préfecture de police des Bouches-du-Rhône</option><option value="prefecture">Préfecture</option><option value="prefecture_greffe_associations">Greffe des associations</option><option value="prefecture_region">Préfecture de région</option><option value="prudhommes">Conseil de prud’hommes</option><option value="rectorat">Rectorat</option><option value="sdac">Service territorial de l’architecture et du patrimoine</option><option value="sdsei">Services départementaux des solidarités et de l'insertion</option><option value="service_navigation">Service de la navigation</option><option value="sgami">Secrétariat pour l'administration du ministère de l'Intérieur</option><option value="sie">Service des impôts des entreprises du Centre des finances publiques</option><option value="sip">Service des impôts des particuliers du Centre des finances publiques</option><option value="sous_pref">Sous-préfecture</option><option value="spip">Service pénitentiaire d'insertion et de probation</option><option value="suio">Service universitaire d'information et d'orientation</option><option value="ta">Tribunal administratif</option><option value="te">Tribunal pour enfants</option><option value="tgi">Tribunal de grande instance</option><option value="ti">Tribunal d’instance</option><option value="tresorerie">Trésorerie</option><option value="tribunal_commerce">Tribunal de commerce</option><option value="urssaf">Urssaf</option>
          </select>
        </div>

        <button onClick={this.researchAdmin}>Rechercher une administration</button>

        <div id="RSS">
     
        </div>
      </div>
    );
  }
}


export default App;
